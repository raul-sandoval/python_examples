from openpyxl.workbook import Workbook
from openpyxl import load_workbook

# initialize workbook
workbook = Workbook()
# set active sheet from active workbook
worksheet = workbook.active

# create new sheets
ws1 = workbook.create_sheet('New sheet 1')
ws2 = workbook.create_sheet('New sheet 2')

# change worksheet title
worksheet.title = 'My title'

# print(workbook.sheetnames)

# initialize workbook from existing file
workbook2 = load_workbook('regions.xlsx')

# initialize new sheet and add to workbook2
new_sheet = workbook2.create_sheet('new_sheet')

# set workbook2 active sheet to variable
active_sheet = workbook2.active

# set cell object A1 to variable
cell = active_sheet['A1']

# print cell value
print(cell.value)

# change cell value
active_sheet['A1'] = 0

# save modified wookbook as new book
workbook2.save('modified.xlsx')