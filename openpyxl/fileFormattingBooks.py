from openpyxl.styles import Font,colors,Color,Alignment,PatternFill,GradientFill,Border,Side
from openpyxl.styles import NamedStyle
from openpyxl.workbook import Workbook

workbook = Workbook()
worksheet = workbook.active

# select, merge and unmerge cells
for i in range(1,20):
    worksheet.append(range(300))

# worksheet.merge_cells()
# worksheet.unmerge_cells()

# merge and unmerge cell by range
worksheet.merge_cells('A1:B5')
worksheet.unmerge_cells('A1:B5')

# merge cell by limits
worksheet.merge_cells(start_row=2,start_column=2,end_row=5,end_column=5)

# change cell font
cell = worksheet['B2']
cell.font = Font(size=20, italic=True)
cell.value = 'Merged cell'

# change cell alignment
cell.alignment = Alignment(horizontal='right',vertical='bottom')

# change cell fill
cell.fill = GradientFill(stop=("000000","FFFFFF"))

# change style properties
highlight = NamedStyle(name='highlight')
highlight.font = Font(bold=True)
bd = Side(style='thick',color='000000')
highlight.border = Border(left=bd,top=bd,right=bd,bottom=bd)
highlight.fill = PatternFill('solid',fgColor='FFFF00')

# apply new style configuration to cells
count = 0
for col in worksheet.insert_cols(min_col=8,min_row=1,max_col=30,max_row=30):
        col[count].style = highlight
        count = count + 1
