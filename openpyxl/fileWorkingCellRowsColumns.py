from openpyxl.workbook import Workbook
from openpyxl import load_workbook

# initialize workbook from existing file
workbook = load_workbook('regions.xlsx')

# set active sheet to variable
worksheet = workbook.active

# assign cell range to variable
cell_range = worksheet['A1':'C1']

# assign only one column to variable
column_c = worksheet['C']

# assign column range to variable
column_range = worksheet['A':'C']

# assign range to variable by column and row index
row_range = worksheet[1:5]

for row in worksheet.iter_rows(min_row=1,max_col=3,max_row=3):
    for cell in row:
        print(cell)

for row in worksheet.iter_rows(min_row=1,max_col=3,max_row=3,values_only=True):
    for cell in row:
        print(cell)
