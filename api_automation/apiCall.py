import requests
import json

baseUrl = 'https://api.upcitemdb.com/prod/trial/lookup'
parameters = {'upc': '885909950805'}

response = requests.get(baseUrl,parameters)
# print(response.url)

content = response.content
# print(content)

infoContent = json.loads(content)
# print(infoContent)

item = infoContent['items']
itemInfo = item[0]
title = itemInfo['title']
brand = itemInfo['brand']
print(title)
print(brand)