import pandas as pd
from openpyxl.workbook import Workbook

df_csv = pd.read_csv('Names.csv', header=None)
df_csv.columns = ['First', 'Last', 'Address', 'City', 'State', 'Area Code','CP']

# print data frame columns as array
print(df_csv.columns)

# print one data frame column by index name
print(df_csv['Last'])

# print multiple data frame column by index name
print(df_csv[['Last','State']])

# print only some row values from one column
print(df_csv['Last'][0:3])

# print only index 1 row values from one column and print as list
print(df_csv.iloc[1])

# print value based on index
print(df_csv.iloc[2,1])

# extract especific column values and saved on another data frame
wanted_values = df_csv[['First', 'Last', 'State']]

# save data frame as xlsx file
stored = wanted_values.to_excel('State_Location.xlsx',index=None)
