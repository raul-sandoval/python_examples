# Minim Stepts
# 1.- Assign file on input folder to variable
# 2.- Filter and keep only partnumbers from CN
# 3.- Split each tariff number on separated csv file based on first number

# Extra Stepts
# 1.- Check if output csv files already exists if not created

import pandas as pd
from openpyxl.workbook import Workbook
import pathlib
from os import path
import os

def main():
    # Global variables
    appPath = str(pathlib.Path().absolute()) + '\\'
    inputPath = appPath + 'INPUT\\'
    outputPath = appPath + 'OUTPUT\\'
    
    # call listdir() method
    # path is a directory of which you want to list
    directories = os.listdir(inputPath)
    
    # This would print all the files and directories
    for file in directories:
        file_name, file_extension = os.path.splitext(file)
        if file_extension == ".xlsx":
            tmpExcel = pd.read_excel(inputPath + file)
            tmpExcel = tmpExcel.loc[tmpExcel['Notes'] == 'Only for CN.']
            tmpExcel = tmpExcel[['HTS', '99 HTS']]
            for x in range(0, 10):
                newExcel = tmpExcel.loc[tmpExcel['HTS'].str.startswith(str(x))]
                stored = newExcel.to_excel(outputPath +  str(x) +'.xlsx', index=None)

if __name__ == "__main__":
    main()
    print('script done.')
