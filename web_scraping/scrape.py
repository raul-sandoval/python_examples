import requests
from bs4 import BeautifulSoup

# request data from webpage and save on variable
url = "http://quotes.toscrape.com/"
response = requests.get(url)

# get the HTML part of the response from webpage
soup = BeautifulSoup(response.text,'lxml')
# print(soup)

# search all span tags by class and save on variable
quotes = soup.find_all('span',class_='text')
authors = soup.find_all('small',class_='author')
tags = soup.find_all('div',class_='tags')

# print(quotes)

for i in range(0, len(quotes)):
    print(quotes[i].text + "\n" + authors[i].text)
    quoteTags = tags[i].find_all('a', class_='tag')
    for quoteTag in quoteTags:
        print(quoteTag.text)
    print("-----------------------------") 
